import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class StaticContent extends StatefulWidget {
  const StaticContent({super.key});

  @override
  State<StaticContent> createState() => _StaticContentState();
}

class _StaticContentState extends State<StaticContent> {
  bool obscureText = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            Container(
              height: 50,
              width: 500,
              decoration: const BoxDecoration(
                  //color: Color.fromRGBO(247, 244, 255, 1),
                  ),
              child: Column(
                children: [
                  Row(
                    children: [
                      const SizedBox(
                        width: 20,
                      ),
                      Image.asset('assets/images/image39.png'),
                    ],
                  ),
                ],
              ),
            ),
            // const SizedBox(
            //   height: 20,
            // ),
            const Row(
              children: [
                SizedBox(
                  width: 20,
                ),
                Text(
                  'About eWallet',
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 40,
            ),
            const Row(
              children: [
                SizedBox(
                  width: 20,
                ),
                Text(
                  'Our app allows you to easily store, manage, and spend \nyour money on the go. With our secure platform, you \ncan make transactions, check your balance, and track \nyour spending all in one place.',
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            const Row(
              children: [
                SizedBox(
                  width: 20,
                ),
                Text(
                  'Whether youre paying bills, shopping online, or \nsending money to friends and family, our app makes it \neasy and convenient to do so. Plus, with our various \npromotions and discounts, you can save even more \nwhile using our app.',
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            const Row(
              children: [
                SizedBox(
                  width: 20,
                ),
                Text(
                  'Thank you for choosing us as your preferred e-wallet \nsolution. If you have any questions or feedback, \nplease dont hesitate to contact us. Were always here \nto help.',
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
