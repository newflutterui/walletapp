import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wallet_application/CreateAccount.dart';

class Login extends StatefulWidget {
  const Login({super.key});

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            Container(
              height: 300,
              width: 500,
              decoration: const BoxDecoration(
                color: Color.fromRGBO(247, 244, 255, 1),
              ),
              child: Column(
                children: [
                  Image.asset('assets/images/image1.png'),
                  const SizedBox(
                    height: 50,
                  ),
                  Image.asset('assets/images/image3.png'),
                ],
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            const Row(
              children: [
                SizedBox(
                  width: 20,
                ),
                Text(
                  'Enter your \nmobile number',
                  style: TextStyle(
                    fontSize: 21,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            const Row(
              children: [
                SizedBox(
                  width: 20,
                ),
                Text(
                  'Mobile number',
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ],
            ),
            Container(
              height: 45,
              width: 450,
              decoration: const BoxDecoration(
                color: Color.fromRGBO(225, 227, 237, 1),
              ),
              child: Row(
                children: [
                  const SizedBox(
                    width: 10,
                  ),
                  Container(
                    width: 80,
                    child: Image.asset('assets/images/image17.png'),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  const Expanded(
                    child: TextField(
                      decoration: InputDecoration(
                        // border: OutlineInputBorder(
                        //   borderRadius: BorderRadius.circular(8.0),
                        // ),
                        hintText: '7X-XXXXXXX',
                      ),
                      keyboardType: TextInputType.phone,
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const CreateAccount(),
                  ),
                );
              },
              child: Container(
                height: 45,
                width: 450,
                decoration: BoxDecoration(
                  color: const Color.fromRGBO(87, 50, 191, 1),
                  borderRadius: BorderRadius.circular(5),
                ),
                child: const Center(
                  child: Text(
                    'Continue',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 14,
                        fontWeight: FontWeight.w600),
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            const Row(
              children: [
                SizedBox(
                  width: 60,
                ),
                SizedBox(
                  height: 2,
                  width: 120,
                  child: Drawer(),
                ),
                SizedBox(
                  width: 20,
                ),
                Text(
                  'or continue using',
                  style: TextStyle(
                    color: Color.fromRGBO(120, 131, 141, 1),
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                SizedBox(
                  width: 20,
                ),
                SizedBox(
                  height: 2,
                  width: 120,
                  child: Drawer(),
                ),
              ],
            ),
            const SizedBox(
              height: 30,
            ),
            Container(
              height: 45,
              width: 450,
              child: Image.asset('assets/images/image10.png'),
            ),
            const SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }
}
