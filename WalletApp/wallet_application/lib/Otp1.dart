import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:wallet_application/Otp2.dart';

class Otp1 extends StatefulWidget {
  const Otp1({super.key});

  @override
  State<Otp1> createState() => _Otp1State();
}

class _Otp1State extends State<Otp1> {
  bool obscureText = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            Container(
              height: 50,
              width: 500,
              decoration: const BoxDecoration(
                  //color: Color.fromRGBO(247, 244, 255, 1),
                  ),
              child: Column(
                children: [
                  Row(
                    children: [
                      const SizedBox(
                        width: 20,
                      ),
                      Image.asset('assets/images/image49.png'),
                      const Spacer(),
                      Image.asset('assets/images/image1.png'),
                      // const SizedBox(
                      //   width: 20,
                      // ),
                      const Spacer(
                        flex: 2,
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            const Text(
              'An SMS sent to your mobile number \n               +962 79 XXX-XXXX',
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w600,
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            const Text(
              'Enter six-digit code',
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w400,
                color: Color.fromRGBO(120, 131, 141, 1),
              ),
            ),
            Container(
              height: 70,
              width: 100,
              child: const TextField(
                decoration: InputDecoration(
                  hintText: "XXX-XXX",
                ),
                keyboardType: TextInputType.phone,
              ),
            ),
            const Row(
              children: [
                Spacer(),
                Text(
                  'Resend code',
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: Color.fromRGBO(120, 131, 141, 1),
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  '00:32',
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                Spacer(
                  flex: 1,
                ),
              ],
            ),
            const SizedBox(
              height: 200,
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const Otp2(),
                  ),
                );
              },
              child: Container(
                height: 45,
                width: 450,
                decoration: BoxDecoration(
                  color: const Color.fromRGBO(87, 50, 191, 1),
                  borderRadius: BorderRadius.circular(4),
                ),
                child: const Center(
                  child: Text(
                    'Done',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 14,
                        fontWeight: FontWeight.w600),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
