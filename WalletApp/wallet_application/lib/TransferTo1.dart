// TransferTo1
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:wallet_application/TransferTo2.dart';

class TransferTo1 extends StatefulWidget {
  const TransferTo1({super.key});

  @override
  State<TransferTo1> createState() => _TransferTo1State();
}

class _TransferTo1State extends State<TransferTo1> {
  bool obscureText = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const SizedBox(
            height: 20,
          ),
          Row(
            children: [
              const SizedBox(
                width: 20,
              ),
              Image.asset('assets/images/image49.png'),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          const Text(
            'Transfer to',
            style: TextStyle(
              fontSize: 24,
              fontWeight: FontWeight.w400,
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            height: 72,
            width: 300,
            child: Row(
              children: [
                const SizedBox(
                  width: 30,
                ),
                Container(
                  height: 72,
                  width: 72,
                  child: Image.asset('assets/images/image56.png'),
                ),
                const SizedBox(
                  width: 10,
                ),
                const Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Ali Ahmed',
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    Text(
                      '+1-300-555-0161',
                      style: TextStyle(
                        color: Color.fromRGBO(120, 131, 141, 1),
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 40,
          ),
          const Text(
            'Enter Amount',
            style: TextStyle(
              color: Color.fromRGBO(120, 131, 141, 1),
              fontSize: 12,
              fontWeight: FontWeight.w600,
            ),
          ),
          Container(
            height: 54,
            width: 140,
            child: const TextField(
              decoration: InputDecoration(
                hintText: "             00.00",
              ),
              keyboardType: TextInputType.phone,
            ),
          ),
          const SizedBox(
            height: 180,
          ),
          GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const TransferTo2(),
                ),
              );
            },
            child: Container(
              height: 45,
              width: 450,
              decoration: BoxDecoration(
                color: const Color.fromRGBO(87, 50, 191, 1),
                borderRadius: BorderRadius.circular(4),
              ),
              child: const Center(
                child: Text(
                  'Done',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                      fontWeight: FontWeight.w600),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
