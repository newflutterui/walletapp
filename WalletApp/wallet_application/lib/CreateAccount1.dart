import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:wallet_application/Otp1.dart';

class CreateAccount1 extends StatefulWidget {
  const CreateAccount1({super.key});

  @override
  State<CreateAccount1> createState() => _CreateAccount1State();
}

class _CreateAccount1State extends State<CreateAccount1> {
  bool obscureText = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            Container(
              height: 200,
              width: 500,
              decoration: const BoxDecoration(
                  //color: Color.fromRGBO(247, 244, 255, 1),
                  ),
              child: Column(
                children: [
                  Row(
                    children: [
                      const SizedBox(
                        width: 20,
                      ),
                      Image.asset('assets/images/image49.png'),
                      const Spacer(),
                      Image.asset('assets/images/image1.png'),
                      // const SizedBox(
                      //   width: 20,
                      // ),
                      const Spacer(
                        flex: 2,
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            const Row(
              children: [
                SizedBox(
                  width: 20,
                ),
                Text(
                  'Create Account',
                  style: TextStyle(
                    fontSize: 21,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              height: 80,
              width: 450,
              child: Column(
                children: [
                  const Row(
                    children: [
                      SizedBox(
                        width: 2,
                      ),
                      Text(
                        "Name",
                        style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 12,
                        ),
                      ),
                    ],
                  ),
                  TextField(
                    decoration: InputDecoration(
                      hintText: "e.g. John Doe",
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        borderSide: const BorderSide(
                          color: Color.fromRGBO(225, 227, 237, 1),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              height: 80,
              width: 450,
              child: Column(
                children: [
                  const Row(
                    children: [
                      SizedBox(
                        width: 2,
                      ),
                      Text(
                        "Email",
                        style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 12,
                        ),
                      ),
                    ],
                  ),
                  TextField(
                    decoration: InputDecoration(
                      hintText: "e.g. email@example.com",
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        borderSide: const BorderSide(
                          color: Color.fromRGBO(225, 227, 237, 1),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              height: 80,
              width: 450,
              child: Column(
                children: [
                  const Row(
                    children: [
                      SizedBox(
                        width: 2,
                      ),
                      Text(
                        "Password",
                        style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 12,
                        ),
                      ),
                    ],
                  ),
                  TextField(
                    decoration: InputDecoration(
                      hintText: "Enter your password",
                      suffixIcon: const Icon(Icons.visibility_off),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        borderSide: const BorderSide(
                          color: Color.fromRGBO(225, 227, 237, 1),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              children: [
                const SizedBox(
                  width: 20,
                ),
                Checkbox(
                  value: false,
                  onChanged: (value) {},
                ),
                const Text(
                  'I accept terms and conditions and privacy policy',
                  style: TextStyle(
                    fontWeight: FontWeight.w400,
                    fontSize: 12,
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const Otp1(),
                  ),
                );
              },
              child: Container(
                height: 45,
                width: 450,
                decoration: BoxDecoration(
                  color: const Color.fromRGBO(87, 50, 191, 1),
                  borderRadius: BorderRadius.circular(4),
                ),
                child: const Center(
                  child: Text(
                    'Create a new account',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 14,
                        fontWeight: FontWeight.w600),
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            const Row(
              children: [
                SizedBox(
                  width: 60,
                ),
                SizedBox(
                  height: 2,
                  width: 120,
                  child: Drawer(),
                ),
                SizedBox(
                  width: 20,
                ),
                Text(
                  'or continue using',
                  style: TextStyle(
                    color: Color.fromRGBO(120, 131, 141, 1),
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                SizedBox(
                  width: 20,
                ),
                SizedBox(
                  height: 2,
                  width: 120,
                  child: Drawer(),
                ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              height: 45,
              width: 450,
              child: Image.asset('assets/images/image10.png'),
            ),
            const SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }
}
