import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class PaymentSuccess extends StatefulWidget {
  const PaymentSuccess({super.key});

  @override
  State<PaymentSuccess> createState() => _PaymentSuccessState();
}

class _PaymentSuccessState extends State<PaymentSuccess> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const SizedBox(
            height: 60,
          ),
          Center(
            child: Container(
              height: 210,
              width: 210,
              child: Image.asset('assets/images/image43.png'),
            ),
          ),
          const SizedBox(
            height: 40,
          ),
          const Row(
            children: [
              SizedBox(
                width: 20,
              ),
              Text(
                'Payment details',
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            height: 60,
            width: 450,
            decoration: BoxDecoration(
              //color: Colors.white,
              borderRadius: BorderRadius.circular(4),
              border: Border.all(
                color: const Color.fromRGBO(237, 239, 246, 1),
                width: 2,
              ),
            ),
            child: const Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  children: [
                    SizedBox(
                      width: 20,
                    ),
                    Text(
                      "Biller",
                      style: TextStyle(
                        color: Color.fromRGBO(120, 131, 141, 1),
                        fontWeight: FontWeight.w400,
                        fontSize: 12,
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    SizedBox(
                      width: 20,
                    ),
                    Text(
                      "Electricity company inc.",
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 12,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            height: 60,
            width: 450,
            decoration: BoxDecoration(
              //color: Colors.white,
              borderRadius: BorderRadius.circular(4),
              border: Border.all(
                color: const Color.fromRGBO(237, 239, 246, 1),
                width: 2,
              ),
            ),
            child: const Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  children: [
                    SizedBox(
                      width: 20,
                    ),
                    Text(
                      "Amount",
                      style: TextStyle(
                        color: Color.fromRGBO(120, 131, 141, 1),
                        fontWeight: FontWeight.w400,
                        fontSize: 12,
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    SizedBox(
                      width: 20,
                    ),
                    Text(
                      "132.32",
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 12,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            height: 60,
            width: 450,
            decoration: BoxDecoration(
              //color: Colors.white,
              borderRadius: BorderRadius.circular(4),
              border: Border.all(
                color: const Color.fromRGBO(237, 239, 246, 1),
                width: 2,
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Row(
                  children: [
                    SizedBox(
                      width: 20,
                    ),
                    Text(
                      "Transaction no.",
                      style: TextStyle(
                        color: Color.fromRGBO(120, 131, 141, 1),
                        fontWeight: FontWeight.w400,
                        fontSize: 12,
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    const SizedBox(
                      width: 20,
                    ),
                    const Text(
                      "23010412432431",
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 12,
                      ),
                    ),
                    const Spacer(),
                    Container(
                      height: 24,
                      width: 24,
                      child: Image.asset('assets/images/image24.png'),
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                  ],
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 16,
                width: 16,
                child: Image.asset('assets/images/image55.png'),
              ),
              const SizedBox(
                width: 5,
              ),
              const Text(
                "Report a problem",
                style: TextStyle(
                  color: Color.fromRGBO(184, 50, 50, 1),
                  fontWeight: FontWeight.w600,
                  fontSize: 14,
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 60,
          ),
          Container(
            height: 45,
            width: 450,
            decoration: BoxDecoration(
              color: const Color.fromRGBO(87, 50, 191, 1),
              borderRadius: BorderRadius.circular(4),
            ),
            child: const Center(
              child: Text(
                "Back to wallet",
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w600,
                  fontSize: 14,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
