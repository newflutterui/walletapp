import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:wallet_application/PaymentSuccess.dart';

class PayBills1 extends StatefulWidget {
  const PayBills1({super.key});

  @override
  State<PayBills1> createState() => _PayBills1State();
}

class _PayBills1State extends State<PayBills1> {
  bool obscureText = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const SizedBox(
            height: 20,
          ),
          Row(
            children: [
              const SizedBox(
                width: 20,
              ),
              Image.asset('assets/images/image49.png'),
            ],
          ),
          const SizedBox(
            height: 40,
          ),
          const Row(
            children: [
              SizedBox(
                width: 20,
              ),
              Text(
                'Pay to',
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.w400,
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Row(
            children: [
              const SizedBox(
                width: 20,
              ),
              Container(
                height: 48,
                width: 48,
                child: Image.asset('assets/images/image54.png'),
              ),
              const SizedBox(
                width: 10,
              ),
              const Text(
                'New biller',
                style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          const Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              SizedBox(
                height: 3,
                width: 180,
                child: Drawer(),
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                'or',
                style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                  color: Color.fromRGBO(120, 131, 141, 1),
                ),
              ),
              SizedBox(
                width: 10,
              ),
              SizedBox(
                height: 3,
                width: 180,
                child: Drawer(),
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            height: 37,
            width: 450,
            decoration: const BoxDecoration(
                //color: Color.fromRGBO(225, 227, 237, 1),
                ),
            child: Row(
              children: [
                Expanded(
                  child: TextField(
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8.0),
                        borderSide: const BorderSide(
                          color: Color.fromRGBO(186, 194, 199, 1),
                        ),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8.0),
                        borderSide: const BorderSide(
                          color: Color.fromRGBO(186, 194, 199, 1),
                        ),
                      ),
                      hintText: 'Search biller',
                      prefixIcon: const Icon(Icons.search),
                    ),
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          const Row(
            children: [
              SizedBox(
                width: 20,
              ),
              Text(
                'Saved billers',
                style: TextStyle(
                  color: Color.fromRGBO(83, 93, 102, 1),
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          GestureDetector(
            onTap: () {
              showModalBottomSheet(
                context: context,
                builder: (context) {
                  return Container(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Center(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SizedBox(
                            height: 20,
                          ),
                          Row(
                            //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                height: 56,
                                width: 56,
                                child: Image.asset('assets/images/image39.png'),
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              const Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                //mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    "Electricity",
                                    style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 16,
                                    ),
                                  ),
                                  Text(
                                    "Utility",
                                    style: TextStyle(
                                      color: Color.fromRGBO(120, 131, 141, 1),
                                      fontWeight: FontWeight.w400,
                                      fontSize: 14,
                                    ),
                                  ),
                                ],
                              ),
                              const Spacer(),
                              const Text(
                                "Done",
                                style: TextStyle(
                                  color: Color.fromRGBO(29, 98, 202, 1),
                                  fontWeight: FontWeight.w600,
                                  fontSize: 14,
                                ),
                              ),
                              const SizedBox(
                                width: 20,
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          Container(
                            height: 64,
                            width: 450,
                            decoration: BoxDecoration(
                              color: const Color.fromRGBO(255, 246, 246, 1),
                              borderRadius: BorderRadius.circular(4),
                            ),
                            child: const Center(
                              child: Text(
                                "Due: 132.32",
                                style: TextStyle(
                                  color: Color.fromRGBO(184, 50, 50, 1),
                                  fontWeight: FontWeight.w600,
                                  fontSize: 21,
                                ),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          Container(
                            height: 60,
                            width: 450,
                            decoration: BoxDecoration(
                              //color: Colors.white,
                              border: Border.all(
                                color: const Color.fromRGBO(237, 239, 246, 1),
                                width: 2,
                              ),
                              borderRadius: BorderRadius.circular(4),
                            ),
                            child: const Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Row(
                                  children: [
                                    SizedBox(
                                      width: 20,
                                    ),
                                    Text(
                                      "Due date",
                                      style: TextStyle(
                                        color: Color.fromRGBO(120, 131, 141, 1),
                                        fontWeight: FontWeight.w400,
                                        fontSize: 12,
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    SizedBox(
                                      width: 20,
                                    ),
                                    Text(
                                      "December 29, 2022 - 12:32 ",
                                      style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        fontSize: 12,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          Container(
                            height: 60,
                            width: 450,
                            decoration: BoxDecoration(
                              //color: Colors.white,
                              border: Border.all(
                                color: const Color.fromRGBO(237, 239, 246, 1),
                                width: 2,
                              ),
                              borderRadius: BorderRadius.circular(4),
                            ),
                            child: const Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Row(
                                  children: [
                                    SizedBox(
                                      width: 20,
                                    ),
                                    Text(
                                      "Registration no.",
                                      style: TextStyle(
                                        color: Color.fromRGBO(120, 131, 141, 1),
                                        fontWeight: FontWeight.w400,
                                        fontSize: 12,
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    SizedBox(
                                      width: 20,
                                    ),
                                    Text(
                                      "23010412432431",
                                      style: TextStyle(
                                        fontWeight: FontWeight.w600,
                                        fontSize: 12,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => const PaymentSuccess(),
                                ),
                              );
                            },
                            child: Container(
                              height: 45,
                              width: 450,
                              decoration: BoxDecoration(
                                color: const Color.fromRGBO(253, 194, 40, 1),
                                borderRadius: BorderRadius.circular(4),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    height: 16,
                                    width: 16,
                                    child: Image.asset(
                                        'assets/images/image48.png'),
                                  ),
                                  const Text(
                                    "Secure payment",
                                    style: TextStyle(
                                      color: Color.fromRGBO(39, 6, 133, 1),
                                      fontWeight: FontWeight.w600,
                                      fontSize: 14,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 40,
                          ),
                        ],
                      ),
                    ),
                  );
                },
              );
            },
            child: Container(
              height: 36,
              width: 450,
              child: Row(
                children: [
                  Container(
                    height: 32,
                    width: 32,
                    child: Image.asset('assets/images/image39.png'),
                  ),
                  const SizedBox(
                    width: 10,
                  ),
                  const Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    //mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Electricity',
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      Text(
                        'Due:132.32',
                        style: TextStyle(
                          color: Color.fromRGBO(120, 131, 141, 1),
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ],
                  ),
                  const Spacer(),
                  IconButton(
                    onPressed: () {},
                    icon: const Icon(Icons.keyboard_arrow_right),
                  ),
                ],
              ),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          const SizedBox(
            height: 2,
            width: 450,
            child: Drawer(),
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            height: 36,
            width: 450,
            child: Row(
              children: [
                Container(
                  height: 32,
                  width: 32,
                  child: Image.asset('assets/images/image37.png'),
                ),
                const SizedBox(
                  width: 10,
                ),
                const Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  //mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Water',
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    Text(
                      'Due:32.21',
                      style: TextStyle(
                        color: Color.fromRGBO(120, 131, 141, 1),
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ],
                ),
                const Spacer(),
                IconButton(
                  onPressed: () {},
                  icon: const Icon(Icons.keyboard_arrow_right),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          const SizedBox(
            height: 2,
            width: 450,
            child: Drawer(),
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            height: 36,
            width: 450,
            child: Row(
              children: [
                Container(
                  height: 32,
                  width: 32,
                  child: Image.asset('assets/images/image38.png'),
                ),
                const SizedBox(
                  width: 10,
                ),
                const Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  //mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Phone',
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    Text(
                      'All paid',
                      style: TextStyle(
                        color: Color.fromRGBO(120, 131, 141, 1),
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ],
                ),
                const Spacer(),
                IconButton(
                  onPressed: () {},
                  icon: const Icon(Icons.keyboard_arrow_right),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
