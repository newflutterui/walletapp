import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CardPayment extends StatefulWidget {
  const CardPayment({super.key});

  @override
  State<CardPayment> createState() => _CardPaymentState();
}

class _CardPaymentState extends State<CardPayment> {
  bool obscureText = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            const SizedBox(
              height: 20,
            ),
            Container(
              height: 50,
              width: 500,
              decoration: const BoxDecoration(
                  //color: Color.fromRGBO(247, 244, 255, 1),
                  ),
              child: Column(
                children: [
                  Row(
                    children: [
                      const SizedBox(
                        width: 20,
                      ),
                      Image.asset('assets/images/image49.png'),
                    ],
                  ),
                ],
              ),
            ),
            // const SizedBox(
            //   height: 20,
            // ),
            Container(
              height: 250,
              width: 450,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                //color: const Color.fromRGBO(111, 69, 233, 1),
              ),
              child: Image.asset(
                'assets/images/image16.png',
                fit: BoxFit.fill,
              ),
            ),
            const SizedBox(
              height: 100,
            ),
            Container(
              height: 60,
              width: 400,
              child: Column(
                children: [
                  Container(
                    height: 24,
                    width: 24,
                    child: Image.asset('assets/images/image46.png'),
                  ),
                  const Text(
                    'Move near a device to pay',
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      color: Color.fromRGBO(120, 131, 141, 1),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 150,
            ),
            Container(
              height: 45,
              width: 450,
              decoration: BoxDecoration(
                color: const Color.fromRGBO(87, 50, 191, 1),
                borderRadius: BorderRadius.circular(5),
              ),
              child: Row(
                children: [
                  const Spacer(),
                  Container(
                    height: 15,
                    width: 15,
                    child: Image.asset('assets/images/image25.png'),
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  const Text(
                    'QR Pay',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 14,
                        fontWeight: FontWeight.w600),
                  ),
                  const Spacer(
                    flex: 1,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
