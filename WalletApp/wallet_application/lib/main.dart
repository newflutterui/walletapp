import 'package:flutter/material.dart';
import 'package:wallet_application/CardPayment.dart';
import 'package:wallet_application/Cards.dart';
import 'package:wallet_application/CreateAccount.dart';
import 'package:wallet_application/CreateAccount1.dart';
import 'package:wallet_application/History.dart';
import 'package:wallet_application/Home.dart';
import 'package:wallet_application/Login.dart';
import 'package:wallet_application/More.dart';
import 'package:wallet_application/Otp1.dart';
import 'package:wallet_application/Otp2.dart';
import 'package:wallet_application/PayBills1.dart';
import 'package:wallet_application/PaymentFailed.dart';
import 'package:wallet_application/PaymentSuccess.dart';
import 'package:wallet_application/Settings.dart';
import 'package:wallet_application/SplashScreen.dart';
import 'package:wallet_application/StaticContent.dart';
import 'package:wallet_application/Transfer.dart';
import 'package:wallet_application/TransferTo1.dart';
import 'package:wallet_application/TransferTo2.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SplashScreen(),
      
    );
  }
}
