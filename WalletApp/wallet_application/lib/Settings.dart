import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Settings extends StatefulWidget {
  const Settings({super.key});

  @override
  State<Settings> createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  bool obscureText = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const SizedBox(
            height: 20,
          ),
          Row(
            children: [
              const SizedBox(
                width: 20,
              ),
              Image.asset('assets/images/image49.png'),
              const Spacer(),
              const Text(
                'Profile Settings',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                ),
              ),
              const Spacer(
                flex: 2,
              ),
            ],
          ),
          const SizedBox(
            height: 50,
          ),
          Container(
            height: 104,
            width: 104,
            child: Image.asset('assets/images/image52.png'),
          ),
          const SizedBox(
            height: 20,
          ),
          const Text(
            'Abdullah Ghatasheh',
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w600,
            ),
          ),
          const Text(
            'Joined 2 years ago',
            style: TextStyle(
              color: Color.fromRGBO(120, 131, 141, 1),
              fontSize: 14,
              fontWeight: FontWeight.w600,
            ),
          ),
          const SizedBox(
            height: 30,
          ),
          Container(
            height: 36,
            width: 450,
            child: Row(
              children: [
                Container(
                  height: 32,
                  width: 32,
                  child: Image.asset('assets/images/image33.png'),
                ),
                const SizedBox(
                  width: 10,
                ),
                const Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  //mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Full name',
                      style: TextStyle(
                        color: Color.fromRGBO(120, 131, 141, 1),
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    Text(
                      'Abdullah Ghatasheh',
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
                const Spacer(),
                const Text(
                  'Edit',
                  style: TextStyle(
                    color: Color.fromRGBO(29, 98, 202, 1),
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          const SizedBox(
            height: 3,
            width: 450,
            child: Drawer(),
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            height: 36,
            width: 450,
            child: Row(
              children: [
                Container(
                  height: 32,
                  width: 32,
                  child: Image.asset('assets/images/image34.png'),
                ),
                const SizedBox(
                  width: 10,
                ),
                const Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  //mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Mobile',
                      style: TextStyle(
                        color: Color.fromRGBO(120, 131, 141, 1),
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    Text(
                      '+962 79 890 50 14',
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
                const Spacer(),
                const Text(
                  'Edit',
                  style: TextStyle(
                    color: Color.fromRGBO(29, 98, 202, 1),
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          const SizedBox(
            height: 3,
            width: 450,
            child: Drawer(),
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            height: 36,
            width: 450,
            child: Row(
              children: [
                Container(
                  height: 32,
                  width: 32,
                  child: Image.asset('assets/images/image35.png'),
                ),
                const SizedBox(
                  width: 10,
                ),
                const Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  //mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Email',
                      style: TextStyle(
                        color: Color.fromRGBO(120, 131, 141, 1),
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    Text(
                      'abdgfx@gmail.com',
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
                const Spacer(),
                const Text(
                  'Edit',
                  style: TextStyle(
                    color: Color.fromRGBO(29, 98, 202, 1),
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          const SizedBox(
            height: 3,
            width: 450,
            child: Drawer(),
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            height: 36,
            width: 450,
            child: Row(
              children: [
                Container(
                  height: 32,
                  width: 32,
                  child: Image.asset('assets/images/image36.png'),
                ),
                const SizedBox(
                  width: 10,
                ),
                const Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Change password',
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ],
                ),
                const Spacer(),
                IconButton(
                  onPressed: () {},
                  icon: const Icon(Icons.keyboard_arrow_right),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
