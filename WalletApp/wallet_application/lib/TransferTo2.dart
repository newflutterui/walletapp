// TransferTo1
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:wallet_application/PaymentFailed.dart';

class TransferTo2 extends StatefulWidget {
  const TransferTo2({super.key});

  @override
  State<TransferTo2> createState() => _TransferTo2State();
}

class _TransferTo2State extends State<TransferTo2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const SizedBox(
            height: 20,
          ),
          Row(
            children: [
              const SizedBox(
                width: 20,
              ),
              Image.asset('assets/images/image49.png'),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          const Text(
            'Transfer to',
            style: TextStyle(
              fontSize: 24,
              fontWeight: FontWeight.w400,
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            height: 72,
            width: 300,
            child: Row(
              children: [
                const SizedBox(
                  width: 30,
                ),
                Container(
                  height: 72,
                  width: 72,
                  child: Image.asset('assets/images/image56.png'),
                ),
                const SizedBox(
                  width: 10,
                ),
                const Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Ali Ahmed',
                      style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    Text(
                      '+1-300-555-0161',
                      style: TextStyle(
                        color: Color.fromRGBO(120, 131, 141, 1),
                        fontSize: 12,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 40,
          ),
          const Text(
            'Enter Amount',
            style: TextStyle(
              color: Color.fromRGBO(120, 131, 141, 1),
              fontSize: 12,
              fontWeight: FontWeight.w600,
            ),
          ),
          Container(
            height: 54,
            width: 140,
            child: const TextField(
              decoration: InputDecoration(
                hintText: "             00.00",
              ),
              keyboardType: TextInputType.phone,
            ),
          ),
          const SizedBox(
            height: 180,
          ),
          GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const PaymentFailed(),
                ),
              );
            },
            child: Container(
              height: 45,
              width: 450,
              decoration: BoxDecoration(
                color: const Color.fromRGBO(253, 194, 40, 1),
                borderRadius: BorderRadius.circular(4),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    height: 16,
                    width: 16,
                    child: Image.asset('assets/images/image48.png'),
                  ),
                  const SizedBox(
                    width: 5,
                  ),
                  const Text(
                    "Secure payment",
                    style: TextStyle(
                      color: Color.fromRGBO(39, 6, 133, 1),
                      fontWeight: FontWeight.w600,
                      fontSize: 14,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
